<?php

namespace bhr\SM\Entity;


class Settings
{
    /**
     * @var boolean
     */
    protected $active = false;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $apiSecret;

    /**
     * @var string
     */
    protected $owner;

    /**
     * @var array
     */
    protected $tags;

    /**
     * @var array
     */
    protected $removeTags;

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     * @return $this
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = md5(time() . $apiKey);
        return $this;
    }

    /**
     * @return string
     */
    protected function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param string $apiSecret
     * @return $this
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;
        return $this;
    }

    /**
     * @return string
     */
    public function getSha1()
    {
        return sha1($this->getApiKey().$this->getClientId().$this->getApiSecret());
    }

    /**
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return implode(', ', $this->tags);
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $tags = explode(',', $tags);
        foreach ($tags as $key => $tag) {
            $tags[$key] = trim($tag);
        }
        $this->tags = $tags;
    }

    /**
     * @param string $removeTags
     */
    public function setRemoveTags($removeTags)
    {
        $removeTags = explode(',', $removeTags);
        foreach ($removeTags as $key => $tag) {
            $removeTags[$key] = trim($tag);
        }
        $this->removeTags = $removeTags;
    }

    /**
     * @return array
     */
    public function getRemoveTags()
    {
        return implode(', ', $this->removeTags);
    }

}
