<?php

namespace bhr\SM\Services;

use GuzzleHttp\Client as GuzzleClient;
use bhr\SM\Entity\Settings;
use bhr\SM\Exception\SalesManagoException;


class SalesManagoService
{
    const METHOD_UPSERT = "api/contact/upsert",
          METHOD_ADD_EXT_EVENT = "api/contact/addContactExtEvent",
          METHOD_UPDATE_EXT_EVENT = "api/contact/updateContactExtEvent",

          EVENT_TYPE_CART = "CART",
          EVENT_TYPE_PURCHASE = "PURCHASE";

    /** @var GuzzleClient $guzzle */
    public $guzzle;

    /**
     * instantiate guzzle connection
     * @return GuzzleClient
     */
    public function getGuzzleClient(Settings $settings)
    {
        if (!$this->guzzle) {
            $this->guzzle = new GuzzleClient([
                'base_uri' => $settings->getEndpoint(),
                'timeout'  => 5.0,
                'defaults' => [
                    'headers' => [
                        'Accept' => 'application/json, application/json',
                        'Content-Type' => 'application/json;charset=UTF-8'
                    ]
                ]
            ]);
        }
        return $this->guzzle;
    }

    public function checkConnection(Settings $settings)
    {

        try {
            $data = $this->__getDefaultApiData($settings);

            $guzzle = $this->getGuzzleClient($settings);

            $guzzleResponse = $guzzle->request('POST', self::METHOD_UPSERT, array(
                'json' => $data,
            ));

            $rawResponse = $guzzleResponse->getBody()->getContents();

            $response = json_decode($rawResponse, true);
            var_dump($response);
            if (is_array($response)
                && array_key_exists('message', $response)
                && is_array($response['message'])
            ) {
                foreach ($response['message'] as $message) {
                    if ($message == 'Not authenticated') {
                        return false;
                    }
                }
                return true;
            } else {
                return null;
            }
        } catch (\Throwable $ex) {
            return null;
        }
    }

    public function contactUpsert(Settings $settings, $user = array(), $options = array())
    {

        $data = array_merge($this->__getDefaultApiData($settings), array(
            'contact' => $this->__getContactData($user)
        ));


//        if (count($settings->getTags()) > 0) {
//            $data['tags'] = $settings->getTags();
//        }
//
//        if (count($settings->getRemoveTags()) > 0) {
//            $data['removeTags'] = $settings->getRemoveTags();
//        }

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $data[$key] = $value;
            }
        }

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_UPSERT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);
        var_dump($response);
        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('contactId', $response)
            && $response['success'] == true
        ) {
            return $response['contactId'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
//            throw new SalesManagoException('Unable to add contact. '.$message);
        }
    }

    public function contactExtEvent(Settings $settings, $type, $product = array(), $user = array(), $eventId = "")
    {

        $data = array_merge($this->__getDefaultApiData($settings), array(
            'contactEvent' => $this->__getContactEventData($product, $type, $eventId)
        ));

        if (!empty($user)) {
            $data['email'] = $user['email'];
        }

        if (!empty($eventId)) {
            $method = self::METHOD_UPDATE_EXT_EVENT;
        } else {
            $method = self::METHOD_ADD_EXT_EVENT;
        }

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', $method, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);

        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('eventId', $response)
            && $response['success'] == true
        ) {
            return $response['eventId'];
        } else {
            $message = $this->__handleError($rawResponse, $guzzleResponse->getStatusCode());
//            throw new ManagoException('Unable to add contact external event. '.$message);
        }
    }





    protected function __getDefaultApiData(Settings $settings)
    {
        $data = array(
            'clientId' => $settings->getClientId(),
            'apiKey' => $settings->getApiKey(),
            'requestTime' => time(),
            'sha' => $settings->getSha1(),
            'owner' => $settings->getOwner()
        );
        return $data;
    }

    protected function __getContactData($user)
    {
        if (!empty($user)) {
            $contact = array();
            foreach ($user as $key => $value) {
                $contact[$key] = $value;
            }
            return $contact;
        }
    }

    protected function __getContactEventData($product, $type, $eventId)
    {
        $contactEvent = array(
            'date' => 1000 * time(),
            'description' => $product['description'],
            'products' => $product['products'],
            'location' => $product['location'],
            'value' => $product['value'],
            'contactExtEventType' => $type,
            'detail1' => $product['detail1'],
            'detail2' => $product['detail1'],
            'detail3' => $product['detail1'],
        );

        if (!empty($eventId)) {
            $contactEvent['eventId'] = $eventId;
        }

        return $contactEvent;
    }

    protected function __handleError($rawResponse, $code)
    {
        if ($code >= 200 || $code < 400) {
            $array = json_decode($rawResponse, true);
            if ($array && array_key_exists('message', $array)) {
                $message = implode(' | ', $array['message']);
            } else {
                $message = 'No error message with response: '.substr($rawResponse, 0, 500);
            }
        } else {
            $message = 'SALESmanago error '.$code.' with response: '.substr($rawResponse, 0, 250);
        }
        return $message;
    }
}