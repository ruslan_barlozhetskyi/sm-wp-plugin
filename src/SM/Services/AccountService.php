<?php

namespace bhr\SM\Services;

use GuzzleHttp\Client as GuzzleClient;
use bhr\SM\Entity\Settings;

class AccountService
{
    const METHOD_CREATE_ACCOUNT = "/api/account/registerAppstore",
          METHOD_CREATE_LIVE_CHAT = "/api/external/livechat/createLiveChat";

    /** @var GuzzleClient $guzzle */
    public $guzzle;

    /**
     * instantiate guzzle connection
     * @return GuzzleClient
     */

    public function getGuzzleClient(Settings $settings)
    {
        if (!$this->guzzle) {
            $this->guzzle = new GuzzleClient([
                'base_uri' => $settings->getEndpoint(),
                'timeout'  => 5.0,
                'defaults' => [
                    'headers' => [
                        'Accept' => 'application/json, application/json',
                        'Content-Type' => 'application/json;charset=UTF-8'
                    ]
                ]
            ]);
        }
        return $this->guzzle;
    }

    protected function __getDefaultApiData(Settings $settings)
    {
        $data = array(
            'clientId' => $settings->getClientId(),
            'apiKey' => $settings->getApiKey(),
            'requestTime' => time(),
            'sha' => $settings->getSha1(),
            'owner' => $settings->getOwner()
        );
        return $data;
    }

    protected function __getLiveChatData($options)
    {
        $data = array(
            "chat" => array(
                "name" => $options['chat-name'],
                "defaultConsultant" => array(
                    "name" => $options['consultant'],
                    "avatar" => array(
                        "url" => "https://s3-eu-west-1.amazonaws.com/salesmanago/chat/default_avatar.png"
                    )
                ),
                "contactOwner" => array(
                    "email" => $options['email']
                )
            )
        );
        return $data;
    }

    public function createProduct(Settings $settings, $type, $options = array())
    {
        switch ($type) {
            case self::METHOD_CREATE_LIVE_CHAT:
                $productProperties = $this->__getLiveChatData($options);
                break;
            default:
                throw new \Exception('Unsupported data source type');

        }
        $data = array_merge($this->__getDefaultApiData($settings), $productProperties);

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', $type, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);
        var_dump($response);
        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('contactId', $response)
            && $response['success'] == true
        ) {
            $userAccount = array(
                "success" => true,
                "message" => implode(", ",$response['message'])
            );
            return $userAccount;
        } else {
            $userAccount = array(
                "success" => false,
                "message" => implode(", ",$response['message'])
            );
            return $userAccount;
        }
    }

    public function createAccount(Settings $settings, $user = array())
    {
        $data = array_merge($this->__getDefaultApiData($settings), array(
            'email' => $user['email'],
            'password' => $user['password'],
            'tags' => $user['tags'],
            'lang' => $user['lang'],
            'items' => "[{\"name\":\"LIVE_CHAT\"}]"
        ));

        $guzzle = $this->getGuzzleClient($settings);

        $guzzleResponse = $guzzle->request('POST', self::METHOD_REGISTER_ACCOUNT, array(
            'json' => $data,
        ));

        $rawResponse = $guzzleResponse->getBody()->getContents();

        $response = json_decode($rawResponse, true);
        var_dump($response);
        if (is_array($response)
            && array_key_exists('success', $response)
            && array_key_exists('contactId', $response)
            && $response['success'] == true
        ) {
            $apiKey = md5(time() . $response->apiSecret);

            $userAccount = array(
                "clientId" => $response->clientId,
                "apiKey" => $apiKey,
                "sha" => sha1($apiKey . $response->clientId . $response->apiSecret),
                "email" => $user['email']
            );
            $userAccount = base64_encode(json_encode($userAccount));

            $user = array(
                "success" => true,
                "smuaid" => $userAccount,
                "_smid" => $response->clientId,
            );

            return $user;
        } else {
            $userAccount = array(
                "success" => false,
                "message" => implode(", ", $response->message)
            );
            return $userAccount;
        }
    }
}