<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.12.17
 * Time: 15:04
 */


function tableCreate(){
    global $wpdb;

    $sql = "";

    $table_name = $wpdb->prefix . "salesmanago";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name ( ";
    $sql.= " id mediumint(9) NOT NULL AUTO_INCREMENT, ";
    $sql.= " endpoint VARCHAR(255) DEFAULT '' NOT NULL, ";
    $sql.= " clientId VARCHAR(255) DEFAULT '' NOT NULL, ";
    $sql.= " apiSecret VARCHAR(255) DEFAULT '' NOT NULL, ";
    $sql.= " owner varchar(255) DEFAULT '' NOT NULL, ";
    $sql.= " apiKey varchar(255) DEFAULT '' NOT NULL, ";
    $sql.= " PRIMARY KEY  (id) ";
    $sql.=") $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
}