<?php
//add menu to option page
add_action('admin_menu', 'pb_salesmanago_menu_init');


function pb_salesmanago_menu_init() {
    global $sm_client;

    add_menu_page(
        'SALESmanago',
        'SALESmanago',
        'manage_options',
        'salesmanago',
        'pb_salesmanago_login_page',
        'dashicons-cart',
        10);

    if($sm_client->isLogged()){
        add_submenu_page(
            'salesmanago',
            'Ustawienia',
            'Ustawienia',
            'manage_options',
            'salesmanago_option',
            'pb_salesmanago_options_page'
        );

        if($integrations = $sm_client->getIntegrations()){

            if($integrations['woocommerce'])
                add_submenu_page(
                    'salesmanago',
                    'Woocommerce',
                    'Woocommerce',
                    'manage_options',
                    'salesmanago_option_woocommerce',
                    'pb_salesmanago_woocommerce_options'
                );

            if($integrations['contactFormSeven'])
                add_submenu_page(
                    'salesmanago',
                    'Contact Form 7',
                    'Contact Form 7',
                    'manage_options',
                    'salesmanago_option_contact_form_seven',
                    'pb_salesmanago_contact_form_seven_options'
                );
        }
    }

}


function pb_salesmanago_login_page(){
    require_once SM_VIEWS_DIR."loginView.php";
}

//generate option page
function pb_salesmanago_options_page() {
    require_once SM_VIEWS_DIR."clientOptionsView.php";
}

function pb_salesmanago_woocommerce_options(){
    require_once SM_VIEWS_DIR."clientWooCommerceOptionView.php";
}

function pb_salesmanago_contact_form_seven_options(){
    require_once SM_VIEWS_DIR."clientContactFormSevenOptionView.php";
}