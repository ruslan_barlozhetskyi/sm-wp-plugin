<?php
//add the needed scripts and styles

add_action('admin_enqueue_scripts', 'sm_enqueue_admin_scripts');
function sm_enqueue_admin_scripts() {
    wp_enqueue_style('wp-pointer');
    wp_enqueue_script('wp-pointer');
    //hook the pointer
    add_action('admin_print_footer_scripts', 'sm_print_footer_scripts' );
}

function sm_print_footer_scripts() {

    global $sm_client;

    $some = '';

    if(!$sm_client->isLogged()) {
        $some = 'Sign in</b> or <b>Sign Up';
        $linkGoTo = 'admin.php?page=salesmanago';
    }elseif($emptyOptions = SMFunctions::getEmptyOptions()){
        $some = $emptyOptions;
        $linkGoTo = 'admin.php?page=salesmanago_option';
    }

    $pointer_content = '<h3>'. __( 'So close', 'wco-salesmanago'). '...</h3>';
    $pointer_content.= '<p>'.__( 'You forgot about ', 'wco-salesmanago' ).'<b>'.$some.'</b> '. __( 'please enter a valid data to continue.', 'wco-salesmanago'). '</p>';
    ?>

    <script type="text/javascript">
        //<![CDATA[
        function smOnClick(){
            window.location.replace("<?php echo $linkGoTo; ?>")
        }

        jQuery(document).ready( function($) {
            //jQuery selector to point to
            $('.toplevel_page_salesmanago:first-child').pointer({
                content: '<?php echo $pointer_content; ?>',
                //position: 'top',
                position: {
                    edge: 'left',
                    align: 'center',
                },
                close: function() {
                    window.location.replace("<?php echo $linkGoTo; ?>");
                }
            }).pointer('open');

            $(".wp-pointer-buttons a").replaceWith( "<button class='button button-primary button-large' onclick='smOnClick()'><?php echo __( 'Go to options ', 'wco-salesmanago' ); ?></button>" );
        });

        //]]>
    </script>

    <?php
}