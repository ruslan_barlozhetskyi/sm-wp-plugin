<?php

namespace bhr\WP\includes;


    class SMUser
    {
        public $uId;
        public $single = true;

        public function __construct() { }

        //gets user data by user id or by user login info
        public function getUser($uId, $getType = 'id', $user_login = null){

            switch ($getType) {
                case 'login':
                    $sm_current_user = get_user_by( 'login', $user_login);
                    break;
                default:
                    $sm_current_user = \WP_User::get_data_by('id', $uId);
                    break;
            }
            
            $sm_user_email  = $sm_current_user->user_email;
            $sm_user_id     = $sm_current_user->ID;

            $single = $this->single;

            $first_name             = 'billing_first_name';
            $last_name              = 'billing_last_name';
            $billing_city           = 'billing_city';
            $billing_company        = 'billing_company';
            $billing_phone          = 'billing_phone';
            $billing_postcode       = 'billing_postcode';
            $billing_street         = 'billing_address_1';
            $billing_street_number  = 'billing_address_2';
            $billing_country        = 'billing_country';


            $sm_user = array(
                'name'=> get_user_meta($sm_user_id, $first_name, $single) . " " . get_user_meta($sm_user_id, $last_name, $single),
                'email'=> $sm_user_email,
                'city'=> get_user_meta($sm_user_id, $billing_city, $single),
                'company'=> get_user_meta($sm_user_id, $billing_company, $single),
                'phone'=> get_user_meta($sm_user_id, $billing_phone, $single),
                "address"=> array(
                                'postcode'=> get_user_meta($sm_user_id, $billing_postcode, $single),
                                'streetAddress'=> get_user_meta($sm_user_id, $billing_street, $single)." ".get_user_meta($sm_user_id, $billing_street_number, $single),
                                'country'=> get_user_meta($sm_user_id, $billing_country, $single)
                )
            );

            return $sm_user;
        }

        public function getPurchaseNoAccount($id){

            $single = $this->single;//get post meta as single row

            $pbSm_no_account_purchase_order_id = $id;

            $purchase_no_account_email = '_billing_email';
            $purchase_no_account_company = '_billing_company';
            $purchase_no_account_first_name = '_billing_first_name';
            $purchase_no_account_last_name = '_billing_last_name';
            $purchase_no_account_phone = '_billing_phone';
            $purchase_no_account_address_1 = '_billing_address_1';
            $purchase_no_account_address_2 = '_billing_address_2';
            $purchase_no_account_postcode = '_billing_postcode';
            $purchase_no_account_city = '_billing_city';
            $purchase_no_account_country = '_billing_country';


            $sm_user = array(
                'email'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_email, $single),
                'name'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_first_name, $single) . " " . get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_last_name, $single),
                'city'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_city, $single),
                'company'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_company, $single),
                'phone'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_phone, $single),
                "address"=> array(
                    'postcode'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_postcode, $single),
                    'streetAddress'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_address_1, $single). " " .get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_address_2, $single),
                    'country'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_country, $single),
                    'zipCode'=> get_post_meta($pbSm_no_account_purchase_order_id, $purchase_no_account_postcode, $single)
                )
            );


            return $sm_user;
        }

    }